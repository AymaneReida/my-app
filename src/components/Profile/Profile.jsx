import { users } from "../Users/ListUsers";
import { useParams } from "react-router-dom";
import { useForm } from "react-hook-form";
import { onSubmit } from "./Submit";
import Form from "../Form/Form";

function Profile() {
	let { id } = useParams();
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm();
	let user = users.find(checkUser);

	function checkUser(element) {
		return element.id === parseInt(id);
	}

	return (
		<Form
			handleSubmit={handleSubmit}
			onSubmit={onSubmit}
			user={user}
			register={register}
			errors={errors}
			subject={"edit"}
		/>
	);
}

export default Profile;
