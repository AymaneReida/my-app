import { useForm } from "react-hook-form";
import { onSubmit } from "./Submit";
import Form from "../Form/Form";

function AddUser() {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm();
	let user = { id: 0, username: "", age: "", sport: "" };

	return (
		<Form
			handleSubmit={handleSubmit}
			onSubmit={onSubmit}
			user={user}
			register={register}
			errors={errors}
			subject={"add"}
		/>
	);
}

export default AddUser;
