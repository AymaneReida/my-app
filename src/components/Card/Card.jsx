import logo from "../../logo.svg";
import { Link } from "react-router-dom";

function Card(props) {
	return (
		<div className="card mb-3" style={{ width: "18rem", height: "23rem" }}>
			<img src={logo} className="card-img-top" alt="Logo" />
			<div className="card-body">
				<h5 className="card-title">{props.username}</h5>
				<p className="card-text">
					{props.username} is {props.age} year(s) old and play {props.sport}
				</p>
				<Link to={`/profile/${props.id}`}>
					<button className="btn btn-primary float-start w-50">Profile</button>
				</Link>
				<button className="btn btn-danger float-end">
					<i class="bi bi-trash3"></i>
				</button>
			</div>
		</div>
	);
}

export default Card;
