function Pagination({ postsPerPage, currentPage, totalPosts, paginate }) {
	const pageNumbers = [];

	for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
		pageNumbers.push(i);
	}

	return (
		<nav aria-label="Page navigation example">
			<ul className="mt-5 justify-content-md-center pagination">
				<li className="page-item">
					{currentPage > 1 ? (
						<button
							onClick={() => paginate(currentPage - 1)}
							className="page-link"
						>
							Previous
						</button>
					) : null}
				</li>
				{pageNumbers.map((number) => (
					<li key={number} className="page-item">
						<button onClick={() => paginate(number)} className="page-link">
							{number}
						</button>
					</li>
				))}
				<li className="page-item">
					{currentPage < Math.ceil(totalPosts / postsPerPage) ? (
						<button
							onClick={() => paginate(currentPage + 1)}
							className="page-link"
						>
							Next
						</button>
					) : null}
				</li>
			</ul>
		</nav>
	);
}

export default Pagination;
