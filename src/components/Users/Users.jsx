import { useEffect, useState } from "react";
import Card from "../Card/Card";
import Pagination from "./Pagination";
import { users } from "./ListUsers";

function Users({ query }) {
	const [posts, setPosts] = useState([]);
	const [loading, setLoading] = useState(false);
	const [currentPage, setCurrentPage] = useState(1);
	const [postsPerPage] = useState(8);

	useEffect(() => {
		setPosts(users);
	}, []);

	// Get current posts
	const indexOfLastPost = currentPage * postsPerPage;
	const indexOfFirstPost = indexOfLastPost - postsPerPage;
	const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

	// Change page
	const paginate = (pageNumber) => setCurrentPage(pageNumber);

	const listItems = currentPosts
		.filter((asd) => asd.username.toLowerCase().includes(query))
		.map((user) => (
			<div key={user.id} className="col-md-auto">
				<Card {...user} />
			</div>
		));

	return (
		<>
			<div className="row justify-content-md-center">{listItems}</div>
			<Pagination
				postsPerPage={postsPerPage}
				currentPage={currentPage}
				totalPosts={posts.length}
				paginate={paginate}
			/>
		</>
	);
}

export default Users;
