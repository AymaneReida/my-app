import { Link } from "react-router-dom";
import logo from "../../logo.svg";

function Navbar({ setQuery }) {
	return (
		<nav className="navbar navbar-expand-lg bg-light mb-5">
			<div className="container-fluid">
				<Link className="navbar-brand nav-link active" to="/">
					<img
						src={logo}
						alt="Logo"
						width="30"
						height="24"
						class="d-inline-block align-text-top"
					/>
					Home
				</Link>
				<div className="collapse navbar-collapse" id="navbarSupportedContent">
					<ul className="navbar-nav me-auto mb-2 mb-lg-0">
						<li className="nav-item">
							<Link className="nav-link active" aria-current="page" to="/">
								Home
							</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link active" aria-current="page" to="/users">
								Users
							</Link>
						</li>
						<li className="nav-item">
							<Link
								className="nav-link active"
								aria-current="page"
								to="/users/add"
							>
								Add
							</Link>
						</li>
					</ul>
				</div>
				<form class="d-flex" role="search">
					<input
						class="form-control me-2"
						placeholder="Search"
						aria-label="Search"
						onChange={(e) => setQuery(e.target.value.toLowerCase())}
					/>
					<button class="btn btn-outline-success">Search</button>
				</form>
			</div>
		</nav>
	);
}

export default Navbar;
