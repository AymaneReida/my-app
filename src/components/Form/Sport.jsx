function Sport(props) {
	return (
		<div className="mb-3 row">
			<label for="staticSport" className="col-sm-2 col-form-label">
				Sport
			</label>
			<div className="col-sm-8">
				<input
					type="text"
					className="form-control"
					id="staticSport"
					aria-describedby="sportHelp"
					defaultValue={props.sport}
					{...props.register("sport", {
						required: { value: true, message: "This field is required" },
						minLength: { value: 3, message: "min length is 3" },
						maxLength: { value: 15, message: "max length is 10" },
					})}
				/>
				{props.errors.sport && (
					<span className="text-danger">{props.errors.sport.message}</span>
				)}
				<div id="sportHelp" className="form-text">
					We'll never share your sport with anyone else.
				</div>
			</div>
		</div>
	);
}

export default Sport;
