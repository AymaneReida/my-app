function Username(props) {
	return (
		<div className="mb-3 row">
			<label for="staticUsername" className="col-sm-2 col-form-label">
				Username
			</label>
			<div className="col-sm-8">
				<input
					type="text"
					className="form-control"
					id="staticUsername"
					aria-describedby="usernameHelp"
					defaultValue={props.username}
					{...props.register("username", {
						required: { value: true, message: "This field is required" },
						minLength: { value: 10, message: "min length is 10" },
						maxLength: { value: 18, message: "max length is 18" },
					})}
				/>
				{props.errors.username && (
					<span className="text-danger">{props.errors.username.message}</span>
				)}
				<div id="usernameHelp" className="form-text">
					We'll never share your username with anyone else.
				</div>
			</div>
		</div>
	);
}

export default Username;
