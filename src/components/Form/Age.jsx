function Age(props) {
	return (
		<div className="mb-3 row">
			<label for="staticUsername" className="col-sm-2 col-form-label">
				Age
			</label>
			<div className="col-sm-8">
				<input
					type="text"
					className="form-control"
					id="staticAge"
					aria-describedby="ageHelp"
					defaultValue={props.age}
					{...props.register("age", {
						required: { value: true, message: "This field is required" },
						minLength: { value: 1, message: "min length is 1" },
						maxLength: { value: 3, message: "max length is 3" },
					})}
				/>
				{props.errors.age && (
					<span className="text-danger">{props.errors.age.message}</span>
				)}
				<div id="ageHelp" className="form-text">
					We'll never share your age with anyone else.
				</div>
			</div>
		</div>
	);
}

export default Age;
