import Username from "./Username";
import Sport from "./Sport";
import Age from "./Age";

function Form({ handleSubmit, onSubmit, user, register, errors, subject }) {
	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<Username username={user.username} register={register} errors={errors} />
			<Age age={user.age} register={register} errors={errors} />
			<Sport sport={user.sport} register={register} errors={errors} />
			<div className="row">
				<div className="col-sm-10">
					<button type="submit" className="btn btn-success float-end w-25">
						{subject === "edit" ? "Edit" : "Add"}
					</button>
				</div>
			</div>
		</form>
	);
}

export default Form;
