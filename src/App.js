import { Routes, Route } from "react-router-dom";
import Home from "./components/Home/Home";
import Users from "./components/Users/Users";
import Profile from "./components/Profile/Profile";
import Navbar from "./components/Navbar/Navbar";
import AddUser from "./components/AddUser/AddUser";
import { useState } from "react";

function App() {
	const [query, setQuery] = useState("");

	return (
		<div className="container-fluid text-center p-0">
			<header className="row">
				<div className="col">
					<Navbar setQuery={setQuery} />
				</div>
			</header>
			<div className="row">
				<div className="col">
					<Routes>
						<Route path="/" element={<Home />} />
						<Route path="/users" element={<Users query={query} />} />
						<Route path="/users/add" element={<AddUser />} />
						<Route path="/profile/:id" element={<Profile />} />
					</Routes>
				</div>
			</div>
		</div>
	);
}

export default App;
